<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Template;
use RBS\Selifa\IComponent;
use Exception;
use RBS\Selifa\Traits\OptionableSingleton;

define('TGX_CONTENT_BLOCK',1);
define('TGX_CONTENT_VAR',2);
define('TGX_CONTENT_FINALIZE',3);

/**
 * Class TGX
 *
 * @package RBS\Selifa\Template
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class TGX implements IComponent, ITemplateParser
{
    use OptionableSingleton;

    /**
     * @var string
     */
    private $_DefaultPattern = '@(?<BO><!--)*(?<O>{)(?<Flag>[!|/])(?<Inverse>[!])?(?<Key>[a-zA-Z0-9\._-]+)(?:\((?<Parameter>[\S\s]*?)\))?(?<E>})(?<BE>-->)*@';

    /**
     * @var bool
     */
    private $_EnableCache = false;

    /**
     * @var string
     */
    private $_CacheSaltString = '';

    /**
     * @var ITGXCustomRenderer[]
     */
    private $_CustomFunctions = array();

    /**
     * @var array
     */
    private $_Globals = array();

    /**
     * @param array $options
     * @throws Exception
     */
    protected function _OnInit($options)
    {
        if (isset($options['EnableCache']))
            $this->_EnableCache = $options['EnableCache'];

        if (isset($options['CacheSalt']))
            $this->_CacheSaltString = trim($options['CacheSalt']);

        $flowControl = new TGXFlowControl();
        $this->RegisterCustomRenderer($flowControl);

        $assetRegister = TGXAssetRegister::Initialize();
        $this->RegisterCustomRenderer($assetRegister);
    }

    /**
     * @param array $array
     * @return bool
     */
    private function _IsAssociativeArray($array)
    {
        return (array_keys($array) !== range(0, count($array) - 1));
    }

    #region Statics
    /**
     * @param array $options
     * @return null|TGX
     */
    public static function Initialize($options)
    {
        if (self::$_Instance == null)
            self::$_Instance = new TGX($options);
        return self::$_Instance;
    }

    /**
     * GetInstance is Deprecated!
     *
     * @return null|ITemplateParser|TGX
     */
    public static function GetInstance()
    {
        return self::Instance();
    }

    /**
     * @param string $text
     * @param array $variables
     * @param array $options
     * @return string|null
     */
    public static function Parse($text, $variables, $options = array())
    {
        return self::$_Instance->ParseText($text,$variables,$options);
    }
    #endregion

    #region Tokenizer
    /**
     * @param string $source
     * @param string $pattern
     * @param array $parsedStruct
     * @param int $startPos
     * @return array
     */
    private function _TemplateToTokens($source,$pattern,&$parsedStruct,&$startPos)
    {
        $tResult = array('MK'=>'','LastPos'=>0,'EndPos'=>0);
        while (preg_match($pattern, $source, $matched, PREG_OFFSET_CAPTURE, $startPos) > 0)
        {
            $currentKey = trim($matched['Key'][0]);
            $params = array();
            if (isset($matched['Parameter']))
            {
                $xParamString = trim($matched['Parameter'][0]);
                if ($xParamString != '')
                {
                    $temp = explode(',',$xParamString);
                    foreach ($temp as $value)
                    {
                        if ($value[0] == "'")
                            $params[] = array('key'=>null,'value'=>substr($value,1,strlen($value)-2));
                        else
                            $params[] = array('key'=>$value,'value'=>null);
                    }
                }
            }

            $isInverted = false;
            if ($matched['Flag'][0] == '/')
            {
                if (($matched['BO'][0] == '<!--') && ($matched['BE'][0] == '-->'))
                {
                    $tResult = array('MK'=>$currentKey,'LastPos'=>$startPos,'EndPos'=>$matched['BO'][1]);
                    $startPos = ($matched['BO'][1] + strlen('<!--{!'.$matched['Key'][0].'}-->'));
                    break;
                }
            }
            else
            {
                if (isset($matched['Inverse']))
                    $isInverted = ($matched['Inverse'][0] == '!');
            }

            if (($matched['BO'][0] == '<!--') && ($matched['BE'][0] == '-->'))
            {
                $token = array(
                    't_start' => substr($source,$startPos,($matched['BO'][1]-$startPos)),
                    'startpos' => $matched['BO'][1],
                    'type' => TGX_CONTENT_BLOCK,
                    'id' => $matched['Key'][0],
                    'key' => $currentKey,
                    'params' => $params,
                    'inverted' => $isInverted,
                    'content' => '',
                    't_end' => ''
                );
                $startPos = ($matched['BO'][1] + strlen($matched[0][0]));

                $childs = array();
                $mRes = $this->_TemplateToTokens($source,$pattern,$childs,$startPos);
                if ($mRes['MK'] == $token['key'])
                {
                    $token['t_end'] = substr($source,$mRes['LastPos'],($mRes['EndPos']-$mRes['LastPos']));
                    $token['childs'] = $childs;
                    $parsedStruct[] = $token;
                }
            }
            else
            {
                $token = array(
                    't_start' => substr($source,$startPos,($matched['O'][1]-$startPos)),
                    'startpos' => $matched['O'][1],
                    'type' => TGX_CONTENT_VAR,
                    'id' => $matched['Key'][0],
                    'key' => $currentKey,
                    'params' => $params,
                    'inverted' => $isInverted,
                    'content' => '',
                    't_end' => '',
                    'childs' => array()
                );
                $startPos = ($matched['O'][1] + strlen($matched[0][0]));
                $parsedStruct[] = $token;
            }
        }

        return $tResult;
    }

    /**
     * @param string $source
     * @param string $pattern
     * @param array $parsedStruct
     * @param int $startPos
     */
    private function _TemplateToTokensFinalize($source,$pattern,&$parsedStruct,&$startPos)
    {
        $token = array(
            't_start' => substr($source,$startPos),
            'startpos' => $startPos,
            'type' => TGX_CONTENT_FINALIZE,
            'id' => '',
            'key' => '',
            'params' => array(),
            'inverted' => false,
            'content' => '',
            't_end' => '',
            'childs' => array()
        );
        $parsedStruct[] = $token;
    }

    /**
     * @param string $text
     * @return array
     */
    public function Tokenize($text)
    {
        $startPos = 0;
        $pTokens = array(
            't_start' => '',
            'startpos' => $startPos,
            'type' => TGX_CONTENT_BLOCK,
            'id' => '',
            'key' => '',
            'params' => array(),
            'inverted' => false,
            'content' => '',
            't_end' => '',
            'childs' => array()
        );
        $this->_TemplateToTokens($text,$this->_DefaultPattern,$pTokens['childs'],$startPos);
        $this->_TemplateToTokensFinalize($text,$this->_DefaultPattern,$pTokens['childs'],$startPos);
        return $pTokens;
    }
    #endregion

    #region Renderer
    /**
     * @param array $params
     * @param array $vars
     * @return array
     */
    private function _VariableToParameters($params,$vars)
    {
        $result = array();
        foreach ($params as $item)
        {
            if ($item['value'] != null)
                $result[] = $item['value'];
            else if ($item['key'] != null)
            {
                if (isset($vars[$item['key']]))
                    $result[] = $vars[$item['key']];
                else if (isset($this->_Globals[$item['key']]))
                    $result[] = $this->_Globals[$item['key']];
            }
        }
        return $result;
    }

    /**
     * @param array $tokens
     * @param array $vars
     * @return string
     */
    public function Render($tokens,$vars)
    {
        $result = '';
        foreach ($tokens['childs'] as $child)
        {
            $result .= $child['t_start'];
            if ($child['type'] == TGX_CONTENT_BLOCK)
            {
                $tempResult = '';
                $CRO = $this->GetObjectOfRenderFunctions($child['key']);
                if ($CRO !== null)
                {
                    $_params = $this->_VariableToParameters($child['params'],$vars);
                    $ptr = $CRO->RenderBlock($child['key'],$_params,$child,$child['inverted'],$vars);
                    if (is_bool($ptr))
                    {
                        $doRenderBlock = (bool)$ptr;
                        if ($doRenderBlock)
                        {
                            $tempResult .= $this->Render($child,$vars);
                            $tempResult .= $child['t_end'];
                        }
                    }
                    else if (is_string($ptr))
                    {
                        $tempResult .= $ptr;
                    }
                    else if (is_array($ptr))
                    {
                        if (count($ptr) > 0)
                        {
                            if ($this->_IsAssociativeArray($ptr))
                            {
                                $tempResult .= $this->Render($child, $ptr);
                                $tempResult .= $child['t_end'];
                            }
                            else
                            {
                                foreach ($ptr as $ptrRow)
                                {
                                    $tempResult .= $this->Render($child, $ptrRow);
                                    $tempResult .= $child['t_end'];
                                }
                            }
                        }
                    }
                }
                else
                {
                    $vObject = null;
                    if (isset($vars[$child['key']]))
                        $vObject = $vars[$child['key']];
                    else if (isset($this->_Globals[$child['key']]))
                        $vObject = $this->_Globals[$child['key']];

                    if ($vObject !== null)
                    {
                        if (is_object($vObject))
                        {
                            if ($vObject instanceof ITGXObjectRenderer)
                            {
                                $ptr = $vObject->TGXRender($this);
                                if (is_bool($ptr))
                                {
                                    $doRenderBlock = (bool)$ptr;
                                    if ($doRenderBlock)
                                    {
                                        $tempResult .= $this->Render($child,$vars);
                                        $tempResult .= $child['t_end'];
                                    }
                                }
                                else if (is_string($ptr))
                                {
                                    $tempResult .= $ptr;
                                }
                                else if (is_array($ptr))
                                {
                                    if (count($ptr) > 0)
                                    {
                                        if ($this->_IsAssociativeArray($ptr))
                                        {
                                            $tempResult .= $this->Render($child, $ptr);
                                            $tempResult .= $child['t_end'];
                                        }
                                        else
                                        {
                                            foreach ($ptr as $ptrRow)
                                            {
                                                $tempResult .= $this->Render($child, $ptrRow);
                                                $tempResult .= $child['t_end'];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (is_bool($vObject))
                        {
                            if ($child['inverted'])
                                $vObject = !$vObject;
                            if ($vObject)
                            {
                                $tempResult .= $this->Render($child, $vars);
                                $tempResult .= $child['t_end'];
                            }
                        }
                        else if (is_array($vObject))
                        {
                            if (count($vObject) > 0)
                            {
                                if ($this->_IsAssociativeArray($vObject))
                                {
                                    $tempResult .= $this->Render($child, $vObject);
                                    $tempResult .= $child['t_end'];
                                }
                                else
                                {
                                    foreach ($vObject as $vRow)
                                    {
                                        $tempResult .= $this->Render($child, $vRow);
                                        $tempResult .= $child['t_end'];
                                    }
                                }
                            }
                        }
                    }
                }
                $result .= $tempResult;
            }
            else if ($child['type'] == TGX_CONTENT_VAR)
            {
                if ($vars !== null)
                {
                    $CRO = $this->GetObjectOfRenderFunctions($child['key']);
                    if ($CRO != null)
                    {
                        $_params = $this->_VariableToParameters($child['params'],$vars);
                        $result .= $CRO->RenderInline($child['key'],$_params,$child['inverted'],$vars);
                    }
                    else
                    {
                        $eObject = null;
                        if (is_object($vars))
                            $eObject = $vars->{$child['key']};
                        else if (isset($vars[$child['key']]))
                            $eObject = $vars[$child['key']];
                        else if (isset($this->_Globals[$child['key']]))
                            $eObject = $this->_Globals[$child['key']];
                        if (is_object($eObject))
                        {
                            if ($eObject instanceof ITGXObjectRenderer)
                            {
                                $renderResult = $eObject->TGXRender($this);
                                if (is_string($renderResult))
                                    $result .= $renderResult;
                            }
                        }
                        else
                        {
                            $result .= (string)$eObject;
                        }
                    }
                }
            }
            else if ($child['type'] == TGX_CONTENT_FINALIZE)
            {
                //Ignore
            }
        }

        return $result;
    }
    #endregion

    #region Custom Renderer
    /**
     * @param ITGXCustomRenderer $object
     * @throws Exception
     */
    public function RegisterCustomRenderer($object)
    {
        if (!($object instanceof ITGXCustomRenderer))
            throw new Exception('Registered object is not an instance of ITGXCustomRenderer.');
        $object->RegisterRenderer($this);

        $aFunctions = $object->GetRenderFunctionNames();
        foreach ($aFunctions as $fName)
            $this->_CustomFunctions[$fName] = $object;
    }

    /**
     * @param string $key
     * @return null|ITGXCustomRenderer
     */
    public function GetObjectOfRenderFunctions($key)
    {
        if (isset($this->_CustomFunctions[$key]))
            return $this->_CustomFunctions[$key];
        else
            return null;
    }
    #endregion

    #region Global Variables
    /**
     * @param array|string $keyOrArray
     * @param mixed|null $value
     */
    public function RegisterGlobal($keyOrArray,$value=null)
    {
        if (is_array($keyOrArray))
        {
            foreach ($keyOrArray as $key => $v)
                $this->_Globals[$key] = $v;
        }
        else
            $this->_Globals[$keyOrArray] = $value;
    }
    #endregion

    /**
     * @param string $text
     * @param array $variables
     * @param array $options
     * @return string|null
     */
    public function ParseText($text, $variables, $options = array())
    {
        if ($this->_EnableCache && isset($options['name']))
        {
            $cache = \RBS\Selifa\IO\Cache::Instance();
            $tName = ($this->_CacheSaltString.'_tgx_cache_'.$options['name']);
            $tokens = $cache->Read($tName,null);
            if ($tokens === null)
            {
                $tokens = $this->Tokenize($text);
                $cache->Write($tName,$tokens,3600);
            }
        }
        else
            $tokens = $this->Tokenize($text);
        return $this->Render($tokens,$variables);
    }
}
?>