<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Template;

/**
 * Class TGXFlowControl
 * 
 * @package RBS\Selifa\Template
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class TGXFlowControl implements ITGXCustomRenderer
{
    /**
     * @param TGX $tgxObject
     * @return mixed
     */
    public function RegisterRenderer($tgxObject)
    {
        // TODO: Implement RegisterRenderer() method.
    }

    /**
     * @return array
     */
    public function GetRenderFunctionNames()
    {
        return array('if');
    }

    /**
     * @param string $functionName
     * @param array $parameters
     * @param boolean $isInverted
     * @param array $vars
     * @return mixed|null
     */
    public function RenderInline($functionName, $parameters, $isInverted, $vars)
    {
        $pCount = count($parameters);
        if ($functionName == 'if')
        {
            if ($pCount >= 4)
            {
                if ($parameters[0] == $parameters[1])
                    return $parameters[2];
                else
                    return $parameters[3];
            }
            else if ($pCount == 3)
            {
                if (is_bool($parameters[0]))
                {
                    if ($parameters[0])
                        return $parameters[1];
                    else
                        return $parameters[2];
                }
                else
                {
                    if ($parameters[0] == $parameters[1])
                        return $parameters[2];
                }
            }
            else if ($pCount == 2)
            {
                if (is_bool($parameters[0]))
                {
                    if ($parameters[0])
                        return $parameters[1];
                }
            }
        }
        return '';
    }

    /**
     * @param string $functionName
     * @param array $parameters
     * @param array $token
     * @param boolean $isInverted
     * @param array $vars
     * @return mixed|null
     */
    public function RenderBlock($functionName, $parameters, $token, $isInverted, $vars)
    {
        $pCount = count($parameters);
        if ($pCount < 2)
            return false;

        if ($functionName == 'if')
        {
            if ($isInverted)
                return ($parameters[0] != $parameters[1]);
            else
                return ($parameters[0] == $parameters[1]);
        }

        return false;
    }

}
?>