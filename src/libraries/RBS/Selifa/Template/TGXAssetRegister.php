<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Template;

/**
 * Class TGXAssetRegister
 *
 * @package RBS\Selifa\Template
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class TGXAssetRegister implements ITGXCustomRenderer
{
    /**
     * @var null|TGXAssetRegister
     */
    private static $_Instance = null;

    /**
     * @var null|TGX
     */
    private $_Parser = null;

    /**
     * @var array
     */
    private $_Cache = array();

    /**
     * @var array
     */
    private $_PartCaches = array();

    /**
     * @var string
     */
    private $_PartPath = '';


    /**
     * @var array
     */
    private $_InlineFormats = array(
        'style' => '<link rel="stylesheet" type="text/css" href="%s">',
        'script' => '<script src="%s"></script>'
    );

    /**
     * @var array
     */
    private $_BlockFormats = array(
        'style' => '<style type="text/css">%s</style>',
        'script' => '<script>%s</script>'
    );

    /**
     *
     */
    private function __construct()
    {

    }

    /**
     * @param TGX $tgxObject
     * @return mixed
     */
    public function RegisterRenderer($tgxObject)
    {
        $this->_Parser = $tgxObject;
    }

    /**
     * @return array
     */
    public function GetRenderFunctionNames()
    {
        return array('add_asset','flush_asset','define_asset','set_part','write_part','load_part');
    }

    /**
     * @param string $functionName
     * @param array $parameters
     * @param boolean $isInverted
     * @param array $vars
     * @return mixed|null
     */
    public function RenderInline($functionName, $parameters, $isInverted, $vars)
    {
        $pCount = count($parameters);
        if ($pCount <  1)
            return false;

        $assetType = $parameters[0];
        if ($functionName == 'add_asset')
        {
            $id = '';
            if ($pCount >= 3)
            {
                $category = $parameters[1];
                $source = $parameters[2];
                if (isset($parameters[3]))
                    $id = $parameters[3];
            }
            else
            {
                $category = 'default';
                $source = $parameters[1];
                if (isset($parameters[2]))
                    $id = $parameters[2];
            }
            $this->_Cache[$assetType][$category][] = array(
                'source' => $source,
                'id' => $id
            );
        }
        else if ($functionName == 'flush_asset')
        {
            if (isset($parameters[1]))
                $category = $parameters[1];
            else
                $category = 'default';

            if (!isset($this->_Cache[$assetType][$category]))
                return false;

            if (!isset($this->_InlineFormats[$assetType]))
                return false;

            $format = $this->_InlineFormats[$assetType];
            $output = '';
            foreach ($this->_Cache[$assetType][$category] as $item)
                $output .= (sprintf($format,$item['source'],$item['id'])."\n");
            return $output;
        }
        else if ($functionName == 'define_asset')
        {
            if (isset($parameters[1]))
                $this->_InlineFormats[$assetType] = $parameters[1];
        }
        else if ($functionName == 'set_part')
        {
            if (isset($parameters[1]))
                $this->_PartCaches[$assetType] = $parameters[1];
        }
        else if ($functionName == 'write_part')
        {
            if (isset($this->_PartCaches[$assetType]))
                return $this->_PartCaches[$assetType];
        }
        else if ($functionName == 'load_part')
        {
            if (isset($parameters[1]))
            {
                $fileName = ($this->_PartPath.'/'.$parameters[1]);
                $rawContent = file_get_contents($fileName);
                $content = $this->_Parser->ParseText($rawContent,$vars,array(
                    'name' => $fileName
                ));
                $this->_PartCaches[$assetType] = $content;
            }
            else
            {
                $fileName = ($this->_PartPath.'/'.$assetType);
                $rawContent = file_get_contents($fileName);
                return $this->_Parser->ParseText($rawContent,$vars,array(
                    'name' => $fileName
                ));
            }
        }

        return false;
    }

    /**
     * @param string $functionName
     * @param array $parameters
     * @param array $token
     * @param boolean $isInverted
     * @param array $vars
     * @return mixed|null
     */
    public function RenderBlock($functionName, $parameters, $token, $isInverted, $vars)
    {
        $pCount = count($parameters);
        if ($pCount <  1)
            return false;

        $assetType = $parameters[0];
        if ($functionName == 'set_part')
        {
            $parsedBlock = $this->_Parser->Render($token,$vars);
            $this->_PartCaches[$assetType] = ($parsedBlock.$token['t_end']);
        }

        return false;
    }

    #region Statics
    /**
     * @return null|TGXAssetRegister
     */
    public static function Initialize()
    {
        if (self::$_Instance == null)
            self::$_Instance = new TGXAssetRegister();
        return self::$_Instance;
    }

    /**
     * @param string $path
     */
    public static function SetPartPath($path)
    {
        if (self::$_Instance != null)
            self::$_Instance->_PartPath = $path;
    }

    /**
     * @param string $assetType
     * @param string $source
     * @param string $category
     * @param string $id
     */
    public static function Add($assetType,$source,$category='default',$id='')
    {
        if (self::$_Instance != null)
        {
            self::$_Instance->_Cache[$assetType][$category][] = array(
                'source' => $source,
                'id' => $id
            );
        }
    }

    /**
     * @param string $partName
     * @param string $content
     */
    public static function SetPart($partName,$content)
    {
        if (self::$_Instance != null)
            self::$_Instance->_PartCaches[$partName] = $content;
    }

    /**
     * @param string $assetType
     * @param string $format
     */
    public static function DefineInline($assetType,$format)
    {
        if (self::$_Instance != null)
            self::$_Instance->_InlineFormats[$assetType] = $format;
    }

    /**
     * @param string $assetType
     * @param string $format
     */
    public static function DefineBlock($assetType,$format)
    {
        if (self::$_Instance != null)
            self::$_Instance->_BlockFormats[$assetType] = $format;
    }
    #endregion
}
?>